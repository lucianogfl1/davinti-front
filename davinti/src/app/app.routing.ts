import { ModuleWithProviders } from '@angular/core';
import {Routes, RouterModule} from '@angular/router'
import {ConsultaComponent} from './consulta/consulta.component'
import{CadastroComponent} from './cadastro/cadastro.component'
const APP_ROUTES: Routes = [ 
        {path:'cadastrar', component: CadastroComponent},
        {path:'consultar', component: ConsultaComponent},
        {path:'', component: CadastroComponent}
   
];
export const routing: ModuleWithProviders<any> = RouterModule.forRoot(APP_ROUTES);