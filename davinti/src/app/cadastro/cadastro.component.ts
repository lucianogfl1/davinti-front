import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { PrimeNGConfig } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { Contato } from '../contato';
import { ContatoService } from '../contato.service';
@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css'],
  providers: [MessageService]
})
export class CadastroComponent implements OnInit {
  itemFormulario: FormGroup;
  idadeMaxima: number = 999;
  itemGravado: boolean = false;
  tamanhoString: string = "";
  limitaDigitos: boolean = true;
  quantidadeTelefone:number = 1;
  
  
  Listcontato: Contato[]=[];
  contato: Contato ={
    id: 0,
    nome: '',
    idade: 0,
    telefone1: '',
    telefone2: '',
    telefone3: '' 
  } ;

  counter = Array;
  constructor(private formBuilder: FormBuilder, private primengConfig: PrimeNGConfig, private messageService: MessageService, private contatoService: ContatoService) {
    this.itemFormulario = this.formBuilder.group({
      nome: [""],
      idade: [""],
      telefone1: [""],
      telefone2:[""],
      telefone3:[""]
    });
  }
  ngOnInit(): void {
    this.primengConfig.ripple = true;    
    // 
    //  this.contatoService.findbyID("1").subscribe(console.log);
    
  }
  onSubmit() {    
    this.validaFormulario();   
  }
  validaFormulario() {
    this.contato = this.itemFormulario.value;    
    console.log("itemFormulario", this.itemFormulario.value);
    console.log("contato", this.contato);
    this.contatoService.create(this.contato).subscribe(console.log);    
    this.itemGravado = true;
    if (this.itemGravado) {
      this.messageService.add({ key: 'bc', severity: 'success', summary: 'Sucesso', detail: 'Item Gravado', life: 2000 });     
    }
  }
  validaQuantidade() {
    return this.limitaDigitos;
  }
  limitDigits(event: any): boolean {
    this.tamanhoString = (this.itemFormulario.get('idade')?.value).toString();   
      if (this.tamanhoString.length >= 3) {
        this.limitaDigitos = false;
        return false;
      } else {
        this.limitaDigitos = true;
        return true;
      }    
  }
  addFone(){
    if(this.quantidadeTelefone<3){
      this.quantidadeTelefone++;
    }else{
      this.messageService.add({ key: 'bc', severity: 'warn', summary: 'Atenção', detail: 'Não é possível cadastrar mais de três telefones', life: 2000 });
    }   
  }
  getControlTelefone(i:number){    
    return "telefone"+i;
  }
}
