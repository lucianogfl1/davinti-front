import { Telefone } from './../telefone';
import { Contato } from './../contato';
import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';
import { ContatoService } from '../contato.service';


@Component({
  selector: 'app-consulta',
  templateUrl: './consulta.component.html',
  styleUrls: ['./consulta.component.css']
})
export class ConsultaComponent implements OnInit {
  contatos: Contato[] = [];
  telefones: Telefone[]=[];
  contatosNome: Contato[] = [];
  selectedName: Contato;
  selectedPhone: Contato;
  itemDialog: boolean = false;
  contatoSelecionado: Contato = {
    id: 0,
    nome: '',
    idade: 0,
    telefone1: '',
    telefone2: '',
    telefone3: ''
  };
  contato: Contato = {
    id: 0,
    nome: '',
    idade: 0,
    telefone1: '',
    telefone2: '',
    telefone3: ''
  };
  telefone: Telefone ={
    id:0,
    telefone:''
  };
  telefoneSelecionado: any;

  constructor(private contatoService: ContatoService, private messageService: MessageService, private confirmationService: ConfirmationService) {
    this.selectedName = this.contato;
    this.selectedPhone = this.contato;
    this.contatoService.findAll().subscribe((dados) => {
      this.contatos = dados;
      this.updateContatosNome(this.contatos);     
    });
  }

  ngOnInit(): void {

  }
  filtro(): void {
    if (this.selectedName.nome !== '') {
      this.selectedPhone = this.contatosNome[0];
      this.contatoService.findbyID(this.selectedName.id.toString()).subscribe((dados) => {
        this.contatos = [];
        this.contatos.push(dados)
      });
    } else if (this.selectedPhone.telefone1 !== '') {
      this.selectedName = this.contatosNome[0];
      this.contatoService.findbyID(this.selectedPhone.id.toString()).subscribe((dados) => {
        this.contatos = [];
        this.contatos.push(dados)
      });
    } 
    else {
      this.contatoService.findAll().subscribe((dados) => {
        this.contatos = dados;
      });
    }   
  }
  editaContato(contato: Contato) {
    this.contatoSelecionado = contato;
    this.itemDialog = true;

  }
  deletaContato(contato: Contato) {
    this.contatoSelecionado = contato;
    this.messageService.clear();
    this.messageService.add({ key: 'c', sticky: true, severity: 'warn', summary: 'Deseja Apagar?', detail: 'Confirmar apagar o contato ' + contato.nome + '' });
  }
  onConfirm() {
    this.contatoService.delete(this.contatoSelecionado.id.toString()).subscribe((dado) => {
      this.contatoService.findAll().subscribe((dados) => {
        this.contatos = dados;
        this.updateContatosNome(this.contatos);       
      });
    });

    this.messageService.clear('c');
    this.messageService.add({ key: 'bc', severity: 'success', summary: 'Sucesso', detail: 'Contato Apagado', life: 2000 });

    this.contatoSelecionado = this.contato;
  }
  onReject() {
    this.messageService.clear('c');
    this.contatoSelecionado = this.contato;
  }
  onCancel() {
    this.itemDialog = false;
    this.contatoSelecionado = this.contato;
  }
  onSalvar() {
    this.itemDialog = false;
    this.contatoService.update(this.contatoSelecionado, this.contatoSelecionado.id.toString()).subscribe((dado) => {
      this.contatoService.findAll().subscribe((dados) => {
        this.contatos = dados;
        this.updateContatosNome(this.contatos);        
      });
    });
    this.messageService.add({ key: 'bc', severity: 'success', summary: 'Sucesso', detail: 'Contato Alterado', life: 2000 });
    this.contatoSelecionado = this.contato;
  }
  limpar() {
    this.selectedName = this.contatosNome[0];
    this.selectedPhone = this.contatosNome[0];
  }
  updateContatosNome(contatos: Contato[]) {
    this.contatosNome = [];
    this.contatosNome.push(this.contato);
    contatos.forEach((contato) => {
      this.contatosNome.push(contato);
    });
  }  

}
