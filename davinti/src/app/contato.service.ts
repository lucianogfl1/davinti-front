import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Contato } from './contato';
@Injectable({
  providedIn: 'root'
})
export class ContatoService {
  private readonly API ='http://localhost:8080/contatos/';
  
  constructor(private http: HttpClient) { }

  findAll(){
    return this.http.get<Contato[]>(this.API);
  }
  findbyID(id:string){
    return this.http.get<Contato>(this.API+id);
  }
  create(contato:Contato){   
    return this.http.post(this.API,contato);
  }
  update(contato:Contato,id:string){   
    return this.http.put(this.API+id,contato);
  }
  delete(id:string){   
    return this.http.delete(this.API+id);
  }
}
