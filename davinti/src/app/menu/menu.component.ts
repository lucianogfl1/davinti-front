import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  items: MenuItem[];
  activeItem: MenuItem;
  constructor() {
    this.items = [
      // { label: 'Home', icon: 'pi pi-fw pi-home' },    
      { label: 'Cadastrar', icon: 'pi pi-fw pi-file', routerLink: 'cadastrar' },
      { label: 'Consultar', icon: 'pi pi-fw pi-pencil', routerLink: 'consultar' },
    ];
    this.activeItem = this.items[0];

  }

  ngOnInit(): void {
  } 

}
